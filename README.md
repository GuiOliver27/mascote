# Mascotes
É um subprojeto do projeto principal DuZeruOS (DuZeru), cujo visa desenvolver
ativos digitais tais como arquivos .svg, .webp, .pdf, .ico, .jpg, .png, .mp3,
.mp4, e arquivos de texto plano a conter qualquer tipo de informação pertinente
e relevante ao projeto principal, desde que seja sobre a elaboração dos mascotes.  
  
---  
  
### Os mascotes
Personagens digitalmente criados pela comunidade DZOS (DuZeru). E ao seguir um dos
conceitos do projeto principal DuZeroOS (DuZeru) no qual esta comunidade entende
que tudo tem seu "ponto zero", um começo, então é lembrado que a fauna e a flora nacionais
estão aqui neste continente desde milhares de anos antes de nós humanos, e principalmente
são seres vivos que fazem parte da base de nossa sobrevivência no equilíbrio natural
do ecossistema.

Sendo assim, a comunidade DZOS (DuZeru) considera uma honra poder representar o
projeto DuzeruOS através de algumas espécies de nossa fauna e flora e de suas
características especiais que os fazem únicos.

Por valorizar o tema e na intenção de contribuir socialmente para tal, a comunidade
DZOS (DuZeru) escolhe especialmente aquelas espécies da fauna e flora brasileira
que estão próximos do processo, ou já em processo de extinção.  

---  
  
### Ativos
- Tucano (DuZeru 1.0): .png, .jpg;
- Arara Azul (DuZeru 2.0): .png, .jpg;
- Lobo Guará (DuZeru 2.2): .png, .jpg;
- Tatu-bola  (DuZeru 3.0): .png, .jpg, .svg;
- Gato Maracajá  (DuZeru 4.0): .png, .svg, .mp4.  

  
  
|**Arquivo**                     |**Descrição de conteúdo** |
|:-------------------------------|:-------------------------|
|dzos_1.0_tucano_origem.png      |Imagem original, modelo fonte. Nunca alterar este arquivo.|
|dzos_1.0_tucano.svg             |Imagem vetorial apenas do tucano ou parte dele. Editável. Utilizável decoração em qualquer parte do projeto principal e subprojetos.|
|dzos_1.0_tucano_logo.svg        |Imagem vetorial do tucano, da logo do projeto nesta versão, e guarnições. Editável. Utilizável no `plymouth` e demais decorações em qualquer parte do projeto principal e subprojetos.|
|dzos_2.0_arara_azul_origem.png  |Imagem original, modelo fonte. Nunca alterar este arquivo.|
|dzos_2.0_arara_azul.svg         |Imagem vetorial apenas da arara ou parte dela. Editável.Utilizável decoração em qualquer parte do projeto principal e subprojetos.|
|dzos_2.0_arara_azul_logo.svg    |Imagem vetorial da arara, da logo do projeto nesta versão, e guarnições. Editável. Utilizável no `plymouth` e demais decorações em qualquer parte do projeto principal e subprojetos.|
|dzos_2.2_lobo_guara_origem.png  |Imagem original, modelo fonte. Nunca alterar este arquivo.|
|dzos_2.2_lobo_guara.svg         |Imagem vetorial apenas do lobo ou parte dele. Editável. Utilizável decoração em qualquer parte do projeto principal e subprojetos.|
|dzos_2.2_lobo_guara_logo.svg    |Imagem vetorial do lobo, da logo do projeto nesta versão, e guarnições. Editável. Utilizável no `plymouth` e demais decorações em qualquer parte do projeto principal e subprojetos.|
|dzos_3.0_tatu_bola_origem.png   |Imagem original, modelo fonte. Nunca alterar este arquivo.|
|dzos_3.0_tatu_bola.svg          |Imagem vetorial apenas do tatu ou parte dele. Editável. Utilizável decoração em qualquer parte do projeto principal e subprojetos.|
|dzos_3.0_tatu_bola_logo.svg     |Imagem vetorial do tatu, da logo do projeto nesta versão, e guarnições. Editável. Utilizável no `plymouth` e demais decorações em qualquer parte do projeto principal e subprojetos.|
|dzos_4.0_gato_maracaja_origem.png|Imagem original, modelo fonte. Nunca alterar este arquivo.|
|dzos_4.0_gato_maracaja.svg      |Imagem vetorial apenas do cato ou parte dele. Editável. Utilizável decoração em qualquer parte do projeto principal e subprojetos.|
|dzos_4.0_gato_maracaja_logo.svg |Imagem vetorial do gato, da logo do projeto nesta versão, e guarnições. Editável. Utilizável no `plymouth` e demais decorações em qualquer parte do projeto principal e subprojetos.|
|dzos_4.0_gato_maracaja_apresent.mp4 |Video a conter qualquer tipo de imagem do gato. Editável. Utilizável em vídeos de apresentação do projeto.|